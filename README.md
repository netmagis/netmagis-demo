Demonstration application
=========================

This repository contains scripts used to create the
Netmagis demonstration site.

To automatically redeploy the 2.3 Netmagis demonstration, insert
into the crontab:
    12 * * * * sh netmagis-demo/mkdemo-23.sh

To automatically redeploy the 3.0 Netmagis demonstration, insert
into the crontab:
    24 * * * * sh netmagis-demo/mkdemo-30.sh
