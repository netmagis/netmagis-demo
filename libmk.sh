
###############################################################################
# Utility functions
#

die ()
{
    echo "$1" >&2
    exit 1
}

usage ()
{
    die "usage: $0"
}

check_packages ()
{
    local pkg

    for pkg in $DEPS_PKG
    do
	dpkg-query -W $pkg > /dev/null 2>&1 || die "Package $pkg not found"
    done
}

##############################################################################
# $1: localrepo
# $2: repo
# $3: branch

fetch_netmagis ()
{
    local localrepo="$1" distrepo="$2" branch="$3"

    if [ -d $localrepo ]
    then
	cd $localrepo
	git pull
    else
	mkdir $localrepo || die "Cannot create $localrepo"
	cd $localrepo/..
	git clone $distrepo $localrepo
	cd $localrepo
    fi
    git checkout $branch
}

##############################################################################
# $1: localrepo
# $2: branch

has_changed ()
{
    local localrepo="$1" branch="$2"
    local lastfile="$localrepo/LAST.$branch"
    local last prev
    local r

    if [ -f $lastfile ]
    then prev=$(cat $lastfile)
    else prev=""
    fi

    cd $localrepo
    git log --pretty=format:'%H' -n 1 > $lastfile
    last=$(cat $lastfile)

    if [ "$last" = "$prev" ]
    then r=1
    else r=0
    fi

    return $r
}

##############################################################################

get_tcl_conf ()
{
    tclsh <<'EOF'
	set d [::tcl::pkgconfig get libdir,install]
	puts [file join $d tclConfig.sh]
EOF
}

