#!/bin/sh

#
# Deploy or update a Netmagis 3.0 demo
#
# Usage:
#	mkdemo-30.sh		(without any arg: see config variables below)
#
# This script:
# - needs the following directories at a minimum
#	/netmagis/
#	/netmagis/repos/
#	/netmagis/db/
# - creates /netmagis/nm30 if needed with a complete Netmagis installation
#	or updates it
# - creates /netmagis/repos/nm30 if needed with a copy of the gitlab repo
#	or updates it. It stores the last commit in
#	/netmagis/repos/nm30/LAST.3.0.
# - creates /netmagis/db/nm30.dump if needed.
#
# Note: this script assumes that recent versions of nodejs and npm
# packages are installed (see Netmagis 3.0 documentation).
#
# TODO : start or restart /netmagis/nm30/bin/netmagis-restd
#

set -u				# error if undefined variable
set -e				# exit if a command fails

###############################################################################
# Configuration variables
#

LIBPATH=$(dirname $0)
DISTREPO=http://gitlab.com/netmagis/netmagis
PREFIX=/netmagis/nm30
DESTDIR=/netmagis/tmp30
LOCALREPO=/netmagis/repos/nm30
DUMPDB=/netmagis/db/nm30.dump
BRANCH=rest
NMUSER=nm			# postgresql database user
NMPASS=nmdemo			# postgresql database password
NMDB=nm30			# postgresql database name

# packages needed to run installation and demo site
DEPS_PKG="
	git tclsh tcl-dev postgresql postgresql-pltcl
	libpgtcl tcllib tcltls tcl-thread
	pandoc
	nodejs npm
	apache2 libapache2-mod-scgi
	"

PGOPTIONS="--client-min-messages=warning"
export PGOPTIONS

##############################################################################
# Load utility functions

. $LIBPATH/libmk.sh

##############################################################################
# $1: localrepo
# $2: destdir
# $3: prefix
# $4: dbname
# $5: dbuser
# $6: dbpassword

install_netmagis ()
{
    local localrepo="$1" destdir="$2" prefix="$3"
    local dbname="$4" dbuser="$5" dbpassword="$6"

    #
    # Install netmagis in a temporary directory $destdir/$prefix
    #

    cd $localrepo
    make \
	DESTDIR=$destdir \
    	PREFIX=$prefix \
	TCLSH=$(which tclsh) \
	TCLCONF=$(get_tcl_conf) \
	install-server \
	install-client \
	# do not remove this line

    #
    # Configure netmagis.conf
    #

    sed \
	-e "/dbhost/s/[ 	].*/ localhost/"	\
	-e "/dbname/s/[ 	].*/ $dbname/"		\
	-e "/dbuser/s/[ 	].*/ $dbuser/"		\
	-e "/dbpassword/s/[ 	].*/ $dbpassword/"	\
	-e "/^pwgen/s:[ 	].*: /usr/bin/pwgen:"	\
	-e "/^cafile/s:[ 	].*: /etc/ssl/certs/ca-certificates.crt:" \
	-e "/^pdflatex/s:[ 	].*: /usr/bin/pdflatex:"	\
	-e "/^dot/s:[ 	].*: /usr/bin/dot:"		\
	$destdir/$prefix/etc/netmagis.conf.sample		\
	> $destdir/$prefix/etc/netmagis.conf

    #
    # Move temporary directory to $prefix
    #

    if [ -d $prefix ]
    then
	rm -rf $prefix.old
	mv $prefix $prefix.old
    fi
    mv $destdir/$prefix $prefix
    rm -rf $prefix.old
}

##############################################################################
# $1: $prefix

stop_restd ()
{
    local prefix="$1"
    local pid

    pid=$(ps x | grep '[n]etmagis-restd' | sed -e 's/^ *//' -e 's/ .*//')
    if [ x"$pid" != x ]
    then
	kill -1 "$pid"
    fi
}

##############################################################################
# $1: $prefix

start_restd ()
{
    local prefix="$1"
    local pid

    $prefix/bin/netmagis-restd -a ::1 &
    pid=$!			# we should save this pid in a file
}

##############################################################################
# $1: $prefix
# $2: dump file
# $3: database name
# $4: dbuser
# $5: dbpassword

update_database ()
{
    local prefix="$1" dbdump="$2"
    local dbname="$3" dbuser="$4" dbpassword="$5"

    PGUSER=$dbuser
    PGPASSWORD=$dbpassword
    PGHOST=localhost

    export PGUSER PGPASSWORD PGHOST

    if [ -f "$dbdump" ]
    then
	#
	# Drop old (from a previous run) database if it exists
	#

	local tmpdb="${dbname}tmp"

	dropdb --if-exists $tmpdb

	#
	# Load dump in temporary database
	#

	createdb $tmpdb
	psql --quiet --no-psqlrc \
	    --output=$dbdump.log \
	    --file=$dbdump --dbname=$tmpdb

	#
	# Rename database. If it fails because some script is connected
	# to it, it does not matter. We will try again a few minutes later.
	#

	dropdb --if-exists $dbname
	psql --quiet --no-psqlrc \
	    -c "ALTER DATABASE $tmpdb RENAME TO $dbname" template1

    else
	#
	# First time: install the database from example data.
	# Just in case, drop the current database
	#

	dropdb --if-exists $dbname

	#
	# Import example data in $dbname (see netmagis.conf)
	#

	PATH=$prefix/sbin:$PATH
	export PATH

	cd $prefix/share/examples/netmagis/with-views
	sh run-all.sh

	#
	# Add two users "admin" and "user"
	#

	psql --quiet --no-psqlrc \
	    -c "INSERT INTO global.nmuser (login, present, idgrp)
		    VALUES ('admin', 1, 1), ('user', 1, 2) ;
		UPDATE pgauth.user SET password = '*' ;
		INSERT INTO pgauth.user (login, password, lastname, firstname)
		    VALUES
		    ('admin', '\$1\$K2O1czz6\$pf7LqPtjD4IsqWWmxAuxR.',
				'Admin', 'Example'),
		    ('user', '\$1\$sIue6xXK\$T3Ndgi6Y9udL2wg4eSrmJ/',
				'User', 'Example') ;
		INSERT INTO pgauth.member (login, realm)
		    VALUES ('admin', 'authadmin'),
			    ('admin', 'netmagis'),
			    ('user', 'netmagis') ;" \
		$dbname

	#
	# Keep a copy for future reuse
	#

	pg_dump -f "$dbdump" $dbname

	#
	# Remove a line which bails out in database reloading
	#

	sed -i '/^COMMENT ON EXTENSION/s/^/-- /' $dbdump
    fi
}

##############################################################################
# Main program

if [ $# -ne 0 ]
then usage
fi

check_packages
fetch_netmagis $LOCALREPO $DISTREPO $BRANCH

if has_changed $LOCALREPO $BRANCH
then
    install_netmagis $LOCALREPO "$DESTDIR" "$PREFIX" "$NMDB" "$NMUSER" "$NMPASS"
fi

stop_restd "$PREFIX"
update_database "$PREFIX" "$DUMPDB" "$NMDB" "$NMUSER" "$NMPASS"
start_restd "$PREFIX"

exit 0
